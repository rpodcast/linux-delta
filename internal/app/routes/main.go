/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package routes

import (
  "fmt"
  "github.com/gorilla/mux"
  sc "github.com/gorilla/securecookie"
  uuid "github.com/satori/go.uuid"
  "gitlab.com/Kpovoc/linux-delta/internal/app/distros"
  "gitlab.com/Kpovoc/linux-delta/internal/app/ratings"
  "golang.org/x/crypto/bcrypt"
  "html/template"
  "net/http"
  "strconv"
  "strings"
)

const PATH_HOME = "/"
const PATH_DISTRO = "/distro/{distroName:[a-zA-Z0-9\\-_! ]+}"
const PATH_LOGIN = "/login"
const PATH_LOGOUT = "/logout"
const PATH_RATING = "/rating/{ratingId:[a-fA-F0-9\\-]+}"
const PATH_RATING_DELETE = PATH_RATING + "/delete"

type SessionModel struct {
  Id string
  Username string
}
type HomePage struct {
  Session *SessionModel
  Distros []distros.DistroModel
}
type DistroPage struct {
  Session *SessionModel
  Distro *distros.DistroModel
  RatingsInfo []ratings.RatingsInfoModel
  Ratings []ratings.RatingModel
}
type LoginPage struct {
  Session *SessionModel
  ErrorMsg string
}

var templates *template.Template
var cookieHandler *sc.SecureCookie
var admins map[string]string

func Init(adminList map[string]string) {
  templates = initializeTemplates("resources/templates/")
  cookieHandler = initializeCookieHandler()
  router := mux.NewRouter()
  admins = adminList

  router.HandleFunc(PATH_HOME, homeHandler)
  router.HandleFunc(PATH_LOGIN, loginPageHandler).Methods(http.MethodGet)
  router.HandleFunc(PATH_LOGIN, loginFormHandler).Methods(http.MethodPost)
  router.HandleFunc(PATH_LOGOUT, logoutFormHandler).Methods(http.MethodPost)
  router.HandleFunc(PATH_DISTRO, ratingPageHandler).Methods(http.MethodGet)
  router.HandleFunc(PATH_DISTRO, ratingFormHandler).Methods(http.MethodPost)
  router.HandleFunc(PATH_DISTRO+PATH_RATING_DELETE, ratingDeleteHandler)

  http.Handle("/", router)

  // Static files
  http.Handle(
    "/static/",
    http.StripPrefix(
      "/static/",
      http.FileServer(http.Dir("resources/static"))))
}

func initializeTemplates(tmplRoot string) *template.Template {
  return template.Must(
    template.
      New("main").
      Funcs(
        template.FuncMap {
          "getStarRating": getGetStarRatingFunc(),
          "starCtrl": getStarCtrlFunc(),
        }).
      ParseFiles(
        tmplRoot + "home_page.html",
        tmplRoot + "distro_page.html",
        tmplRoot + "login_page.html",
      ))
}

func initializeCookieHandler() *sc.SecureCookie {
  // TODO: May need to read in hashKey and blockKey from config in order to
  // persist sessions across restarts
  hashKey := sc.GenerateRandomKey(64)
  blockKey := sc.GenerateRandomKey(32)

  return sc.New(hashKey, blockKey)
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
  allDistros, err := distros.GetAll()
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  p := &HomePage {
    Session: getSession(r),
    Distros: allDistros,
  }

  renderHomeTemplate(w, p)
}

func ratingPageHandler(w http.ResponseWriter, r *http.Request) {
  distroName := mux.Vars(r)["distroName"]

  p, err := createDistroPage(distroName, r)

  if  err != nil {
    http.NotFound(w, r)
    return
  }

  renderDistroTemplate(w, p)
}

func ratingFormHandler(w http.ResponseWriter, r *http.Request) {
  distroName := mux.Vars(r)["distroName"]
  if distroName == "" {
    http.NotFound(w, r)
    return
  }

  redirectTarget := fmt.Sprintf("/distro/%s", distroName)

  if err := r.ParseForm(); err != nil {
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }
  id, _ := uuid.NewV4()
  distroId, _ := uuid.FromString(r.FormValue("distroId"))
  username := r.FormValue("name")
  rateOverall, _ := strconv.ParseFloat(r.FormValue("overall"), 64)
  rateWorkstation, _ := strconv.ParseFloat(r.FormValue("workstation"), 64)
  rateServer, _ := strconv.ParseFloat(r.FormValue("server"), 64)
  rateIoT, _ := strconv.ParseFloat(r.FormValue("iot"), 64)
  comment := r.FormValue("comment")

  rating := ratings.RatingModel{
    Id: id,
    DistroId: distroId,
    Username: username,
    RateOverall: float64(rateOverall),
    RateWorkstation: rateWorkstation,
    RateServer: rateServer,
    RateIoT: rateIoT,
    Comment: comment,
  }

  _, _ = ratings.Save(rating)

  http.Redirect(w, r, redirectTarget, http.StatusFound)
}

func ratingDeleteHandler(w http.ResponseWriter, r *http.Request) {
  sess := getSession(r)
  if sess.Username == "" {
    http.Error(
      w,
      "Unauthorized attempt to delete rating.",
      http.StatusUnauthorized,
    )
    return
  }

  distroName := mux.Vars(r)["distroName"]
  if distroName == "" {
    http.NotFound(w, r)
    return
  }

  redirectTarget := fmt.Sprintf("/distro/%s", distroName)

  ratingId, err := uuid.FromString(mux.Vars(r)["ratingId"])
  if err != nil {
    http.NotFound(w, r)
    return
  }

  _, err = ratings.DeleteById(ratingId)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  http.Redirect(w, r, redirectTarget, http.StatusFound)
}

func loginPageHandler(w http.ResponseWriter, r *http.Request) {
  p := &LoginPage{
    Session: getSession(r),
  }

  renderLoginTemplate(w, p)
}

func loginFormHandler(w http.ResponseWriter, r *http.Request) {
  name :=  strings.TrimSpace(r.FormValue("name"))
  unhashedPass := strings.TrimSpace(r.FormValue("pass"))
  hashedPass := admins[name]

  if CheckPasswordHash(unhashedPass, hashedPass) {
    setSession(name, w)
    http.Redirect(w, r, "/", http.StatusFound)
  } else {
    p := &LoginPage{
      ErrorMsg: "Unrecognized username/password combination",
      Session: getSession(r),
    }

    renderLoginTemplate(w, p)
  }
}

func logoutFormHandler(w http.ResponseWriter, r *http.Request) {
  clearSession(w)
  http.Redirect(w, r, "/", http.StatusFound)
}

func getGetStarRatingFunc() func(rating float64) template.HTML {
  emptyStar := `<i class="star-empty"></i>`
  halfStar := `<i class="star-half"></i>`
  fullStar := `<i class="star-full"></i>`

  return func(rating float64) template.HTML {
    if rating <= 0.5 {
      return template.HTML(
        halfStar+emptyStar+emptyStar+emptyStar+emptyStar,
      )
    } else if rating <= 1.0 {
      return template.HTML(
        fullStar+emptyStar+emptyStar+emptyStar+emptyStar,
      )
    } else if rating <= 1.5 {
      return template.HTML(
        fullStar+halfStar+emptyStar+emptyStar+emptyStar,
      )
    } else if rating <= 2.0 {
      return template.HTML(
        fullStar+fullStar+emptyStar+emptyStar+emptyStar,
      )
    } else if rating <= 2.5 {
      return template.HTML(
        fullStar+fullStar+halfStar+emptyStar+emptyStar,
      )
    } else if rating <= 3.0 {
      return template.HTML(
        fullStar+fullStar+fullStar+emptyStar+emptyStar,
      )
    } else if rating <= 3.5 {
      return template.HTML(
        fullStar+fullStar+fullStar+halfStar+emptyStar,
      )
    } else if rating <= 4.0 {
      return template.HTML(
        fullStar+fullStar+fullStar+fullStar+emptyStar,
      )
    } else if rating <= 4.5 {
      return template.HTML(
        fullStar+fullStar+fullStar+fullStar+halfStar,
      )
    } else {
      return template.HTML(
        fullStar+fullStar+fullStar+fullStar+fullStar,
      )
    }
  }
}

func getStarCtrlFunc() func(ctrlName string) template.HTML {
  return func(ctrlName string) template.HTML {
    s5 := starInputAndLabel(ctrlName, "5")
    s4 := starInputAndLabel(ctrlName, "4")
    s3 := starInputAndLabel(ctrlName, "3")
    s2 := starInputAndLabel(ctrlName, "2")
    s1 := starInputAndLabel(ctrlName, "1")

    return template.HTML(
      `<div class="stars">`+s5+s4+s3+s2+s1+`</div>`,
    )
  }
}

func getSession(r *http.Request) *SessionModel {
  sess := &SessionModel{
    Id: "",
    Username: "",
  }

  if cookie, err := r.Cookie("username"); err == nil {
    cookieValue := make(map[string]string)
    if err = cookieHandler.Decode("username", cookie.Value, &cookieValue); err == nil {
      sess.Id = "00000000-0000-0000-0000-000000000000"
      sess.Username = cookieValue["username"]
    }
  }

  return sess
}

func setSession(username string, w http.ResponseWriter) {
  value := map[string]string {
    "username": username,
  }
  if encoded, err := cookieHandler.Encode("username", value); err == nil {
    cookie := &http.Cookie{
      Name: "username",
      Value: encoded,
      Path: "/",
      MaxAge: 3600,
    }
    http.SetCookie(w, cookie)
  }
}

func clearSession(w http.ResponseWriter) {
  cookie := &http.Cookie{
    Name: "username",
    Value: "",
    Path: "/",
    MaxAge: -1,
  }
  http.SetCookie(w, cookie)
}

func createDistroPage(name string, r *http.Request) (*DistroPage, error) {
  distro, err := distros.GetByName(name)
  if err != nil {
    return nil, err
  }

  ratingsInfo, err := ratings.CalcRatingsInfo(distro.Id)
  if err != nil {
    return nil, err
  }

  distroRatings, err := ratings.GetByDistroId(distro.Id)
  if err != nil {
    return nil, err
  }

  return &DistroPage {
    Session: getSession(r),
    Distro: distro,
    RatingsInfo: ratingsInfo,
    Ratings: distroRatings,
  }, nil
}

func renderHomeTemplate(w http.ResponseWriter, page *HomePage) {
  err := templates.ExecuteTemplate(w, "home_page.html", page)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
  }
}

func renderDistroTemplate(w http.ResponseWriter, page *DistroPage) {
  err := templates.ExecuteTemplate(w, "distro_page.html", page)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
  }
}

func renderLoginTemplate(w http.ResponseWriter, page *LoginPage) {
  err := templates.ExecuteTemplate(w, "login_page.html", page)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
  }
}

func starInputAndLabel(name string, num string) string {
  cn := `"`+`star star-`+num+`" `
  id := `"`+name+`-star-`+num+`" `
  vl := `"`+num+`" `
  nm := `"`+name+`" `

  sb := strings.Builder{}
  sb.WriteString(`<input class=`)
  sb.WriteString(cn)
  sb.WriteString(`id=`)
  sb.WriteString(id)
  sb.WriteString(`type="radio" value=`)
  sb.WriteString(vl)
  sb.WriteString(`name=`)
  sb.WriteString(nm)

  if num == "1" {
    sb.WriteString(`checked `)
  }

  sb.WriteString(`/><label class=`)
  sb.WriteString(cn)
  sb.WriteString(`for=`)
  sb.WriteString(id)
  sb.WriteString(`></label>`)
  return sb.String()
}

func CheckPasswordHash(password, hash string) bool {
  err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
  return err == nil
}