/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package distros

import (
	"github.com/go-pg/pg"
	uuid "github.com/satori/go.uuid"
)

type DistroModel struct {
	tableName 	struct{} `sql:"distros"`
	Id 					uuid.UUID `sql:"id,type:uuid,pk"`
	Name 				string `sql:"name,type:varchar(100),unique"`
	Description string `sql:"description"`
	DescSource 	string `sql:"description_source"`
	Homepage 		string `sql:"homepage,type:varchar(200)"`
	Tags 				string `sql:"tags,type:varchar(400)"`
	LogoImageH 	string `sql:"logo_image_h,type:varchar(200)"`
	LogoImageV 	string `sql:"logo_image_v,type:varchar(200)"`
	LogoBgColor string `sql:"logo_bg_color,type:varchar(7)"`
}

var dr DistroRepository

func Init(db *pg.DB) {
	dr = DistroRepository{ db: db }
}

func Save(model DistroModel) (*DistroModel, error) {
	// Check required fields
	err := isValid(&model)
	if err != nil {
		return nil, err
	}

	// Add new id
	id, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}
	model.Id = id

	// Save to database
	result, err := dr.Create(&model)
	if err != nil {
		return nil, err
	}

	// Return result
	return result, nil
}

func GetAll() ([]DistroModel, error) {
	result, err := dr.Read()
	if err != nil {
		return nil, err
	}

	return result, nil
}

func GetById(id uuid.UUID) (*DistroModel, error) {
	result, err := dr.ReadById(id)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func GetByName(name string) (*DistroModel, error) {
	result, err := dr.ReadByName(name)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func isValid(model *DistroModel) error {
  // TODO: flesh out
	return nil
}
