package conf

import (
  "encoding/json"
  "io/ioutil"
  "log"
)

type Config struct {
  Admins  map[string]string `json:"admins"`
  DB      dbConfig          `json:"db"`
}

type dbConfig struct {
  Address   string `json:"address"`
  Username  string `json:"username"`
  Password  string `json:"password"`
}
func (d *dbConfig) Addr() string {
  return d.Address
}
func (d *dbConfig) User() string {
  return d.Username
}
func (d *dbConfig) Pass() string {
  return d.Password
}

func LoadConfig(path string) Config {
  file, err := ioutil.ReadFile(path)
  if err != nil {
    log.Fatal("Config File Missing. ", err)
  }

  var config Config
  err = json.Unmarshal(file, &config)
  if err != nil {
    log.Fatal("Config Parse Error: ", err)
  }

  return config
}