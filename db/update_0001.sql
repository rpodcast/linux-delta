INSERT INTO distros (id, name, description, description_source, homepage, tags, logo_image_h, logo_image_v, logo_bg_color)
VALUES
(
  '939c57ce-109a-46b1-81c6-8453411f9a64',
  'Pop!_OS',
  'Pop!_OS is a Linux distribution developed by System76 based on Ubuntu ' ||
  'by Canonical, using the GNOME Desktop Environment. It is intended for ' ||
  'use by "developers, makers, and computer science professionals". ' ||
  'Pop!_OS provides full disk encryption by default as well as streamlined '||
  'window management, workspaces, and keyboard shortcuts for navigation.',
  'https://en.wikipedia.org/wiki/System76#Pop.21_OS',
  'https://system76.com/pop',
  'Workstation,Server,IoT,Cloud',
  'pop_os-horizontal.png',
  'pop_os-vertical.png',
  '#ffffff'
)