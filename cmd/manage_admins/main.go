package main

import (
  "bufio"
  "fmt"
  "golang.org/x/crypto/bcrypt"
  "golang.org/x/crypto/ssh/terminal"
  "io"
  "log"
  "os"
  "strings"
  "syscall"
)

const configPath = "./config/main-conf.json"

func main() {
  a := AdminWizard{}
  a.Run()
}

func HashPassword(password string) (string, error) {
  bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
  return string(bytes), err
}

type AdminWizard struct {
  reader io.Reader
  writer io.Writer
  scanner *bufio.Scanner
}

func (a *AdminWizard) prompt(msg string) {
  if _, err := fmt.Fprintf(a.writer, msg); err != nil {
    log.Fatal(err)
  }
}

func (a *AdminWizard) getUserInput() string {
  a.scanner.Scan()
  return strings.TrimSpace(a.scanner.Text())
}

func (a *AdminWizard) getPasswordInput() string {
  bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
  if err != nil {
    log.Fatal(err)
  }

  unhashedPass := strings.TrimSpace(string(bytePassword))
  hashedPass, err := HashPassword(unhashedPass)
  if err != nil {
    log.Fatal(err)
  }

  return hashedPass
}

func (a *AdminWizard) Run() {
  if a.reader == nil {
    a.reader = os.Stdin
  }
  if a.writer == nil {
    a.writer = os.Stdout
  }

  a.scanner = bufio.NewScanner(a.reader)

  a.prompt("Username: ")
  username := a.getUserInput()
  a.prompt("Password: ")
  password := a.getPasswordInput()

  fmt.Println()
  fmt.Println("Username is "+username)
  fmt.Println("Password is "+password)

  fmt.Printf("\nPassword = Moxley is %v\n",
    CheckPasswordHash("Moxley",password))
}