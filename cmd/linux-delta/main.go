/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package main

import (
  "gitlab.com/Kpovoc/linux-delta/internal/app/conf"
  "gitlab.com/Kpovoc/linux-delta/internal/app/db"
  "gitlab.com/Kpovoc/linux-delta/internal/app/distros"
  "gitlab.com/Kpovoc/linux-delta/internal/app/ratings"
  "gitlab.com/Kpovoc/linux-delta/internal/app/routes"
  "log"
  "net/http"
)

const configPath = "./config/main-conf.json"

func main() {
  config := conf.LoadConfig(configPath)
  pg_db := db.Connect(&config.DB)
  distros.Init(pg_db)
  ratings.Init(pg_db)
  routes.Init(config.Admins)

  err := http.ListenAndServe(":8080", nil)

  db.Close(pg_db)

  if err != nil {
    log.Fatal(err)
  }
}

//import (
//  "github.com/gorilla/securecookie"
//  "github.com/gorilla/sessions"
//  "log"
//  "net/http"
//)
//
//var key = securecookie.GenerateRandomKey(32)
//var store = sessions.NewCookieStore(key)
//
//type Database struct {
//  action string
//}
//
//type Router struct {}
//func (r *Router) HandleFunc(pattern string, handler http.HandlerFunc) {
//  http.HandleFunc(pattern, handler)
//}
//
//type EmailSender struct{}
//
//type server struct {
//  db *Database
//  router *Router
//  email EmailSender
//}
//
//func (s *server) routes() {
//  s.router.HandleFunc("/", s.handleIndex())
//}
//
//func (s *server) handleIndex() http.HandlerFunc {
//  return func(w http.ResponseWriter, r *http.Request) {
//    // Get a session. Get() always returns a session, even if empty.
//    session, err := store.Get(r, "session-name")
//    if err != nil {
//      http.Error(w, err.Error(), http.StatusInternalServerError)
//      return
//    }
//
//    // Set some session values.
//    session.Values["foo"] = "bar"
//    session.Values["42"] = 43
//    // Save it before we write to the response/return from the handler.
//    session.Save(r, w)
//  }
//}
//
//func main() {
//  log.Fatal(http.ListenAndServe(":8080", nil))
//}

